import { test, expect } from "@playwright/test";
import { FILTER_OPTIONS } from "../../src/config.ts";
import { getURLs, expectResponseIsOk } from "../util.ts";

const URLs: string[] = getURLs();

const Levels: string[] = FILTER_OPTIONS.items;
for (const Level of Levels) {
  test.describe(`adf-${Level}`, () => {
    for (const URL of URLs) {
      test(URL, async ({ page }) => {
        //Go to URL
        const resp = await page.goto(URL);
        expectResponseIsOk(resp);
        //Check that level filter exists - as some pages may not include one
        //However we may include a level filter later.
        const filterControls = await page.getByRole("combobox", {
          name: "Levels filter",
        });
        const filterControlsCount = await filterControls.count();
        const LevelSpecificContent = await page
          .locator(`[data-adf-section*="${Level}"]`)
          .all();
        const LevelSpecificContentCount = LevelSpecificContent.length;
        const OtherLevelContent = await page
          .locator(`[data-adf-section]:not([data-adf-section*="${Level}"])`)
          .all();
        const OtherLevelContentCount = OtherLevelContent.length;
        if (
          filterControlsCount > 0 &&
          LevelSpecificContentCount + OtherLevelContentCount > 0
        ) {
          //Select Level
          await filterControls.selectOption(`${Level}`);
          for (let i = 0; i < LevelSpecificContentCount; ++i) {
            //Assert that each element returned should NOT have display: none.
            await expect(
              LevelSpecificContent[i],
              `element should be displayed`
            ).not.toHaveCSS("display", "none");
          }
          for (let i = 0; i < OtherLevelContentCount; ++i) {
            //Assert that each element returned should NOT have display: none.
            await expect(
              OtherLevelContent[i],
              `element should not be displayed`
            ).toHaveCSS("display", "none");
          }
        }
      });
    }
  });
}
