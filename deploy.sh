#!/usr/bin/env bash
# Run this script to deploy the app to Codeberg Pages

# Parse cmd arguments

SRC_BRANCH="main"
DEPLOY_BRANCH="pages"
DEV_BUILD="false"

USAGE_MSG="usage: deploy [-h|--help] [-u|--user] [-s|--src SRC_BRANCH] [-d|--deploy DEPLOY_BRANCH] [-t|--test DEV_BUILD]"

while [[ $# > 0 ]]; do
    key="$1"

    case $key in
        -h|--help)
        echo $USAGE_MSG
        exit 0
        ;;
        -u|--user)
        SRC_BRANCH="source"
        DEPLOY_BRANCH="main"
        shift
        ;;
        -s|--src)
        SRC_BRANCH="$2"
        shift
        ;;
        -d|--deploy)
        DEPLOY_BRANCH="$2"
        shift
        ;;
        -t|--test)
        DEV_BUILD="true"
        ;;
        *)
        echo "Option $1 is unknown."
        echo $USAGE_MSG
        exit 0
        ;;
    esac
    shift
done

# Exit if any subcommand fails
set -e

echo "Deploying..."
echo "Source branch: $SRC_BRANCH"
echo "Deploy branch: $DEPLOY_BRANCH"
echo "Development build: $DEV_BUILD"

read -r -p "Do you want to proceed? [y/N] " response
if [[ ! $response =~ ^([yY][eE][sS]|[yY])+$ ]]
then
    echo "Aborting."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi

# Check if there are any uncommitted changes
if ! git diff-index --quiet HEAD --; then
    echo "Changes to the following files are uncommitted:"
    git diff-index --name-only HEAD --
    echo "Please commit the changes before proceeding."
    echo "Aborting."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi

# Switch to source branch (creates it if necessary from the current branch)
if [ `git branch | grep $SRC_BRANCH | tr ' ' '\n' | tail -1` ]
then
    git checkout $SRC_BRANCH
else
    git checkout -b $SRC_BRANCH
fi

# Checkout DEPLOY_BRANCH branch
if [ `git branch | grep $DEPLOY_BRANCH` ]
then
  git branch -D $DEPLOY_BRANCH
fi
git checkout -b $DEPLOY_BRANCH

# Build site
echo "Ensuring dependencies are installed..."
npm install

echo "Building site..."
if [[ -n $DEV_BUILD ]];
then
    npm run build-dev
else
    npm run build
fi

# Delete and move files
## These files/folders are kept
echo "Moving files..."
find . -maxdepth 1 ! -name 'dist' ! -name '.git' ! -name 'CNAME' ! -name ".gitignore" !  -name 'LICENSE' ! -name "deploy.sh" ! -name "deploy-develop.sh" -exec rm -rf {} \;
## everything in the dist folder is moved to the root, then the dist folder is deleted. This is so that file paths are maintained when they become URLs
mv dist/* .
rm -R dist/

# Push to DEPLOY_BRANCH
echo "Pushing to git..."
git add -fA
git commit --allow-empty -m "$(git log -1 --pretty=%B) [ci skip]"
git push -f -q origin $DEPLOY_BRANCH

# Move back to SRC_BRANCH
git checkout $SRC_BRANCH

echo "Deployed successfully! Re-installing dependencies..."

# The node_modules must be re-installed as they were removed during the deploy
npm install

echo "Finished."
exit 0