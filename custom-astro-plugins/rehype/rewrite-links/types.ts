import type { HastElement, HastNode } from "hast-util-to-text";

// from withastro/astro/packages/astro-rss/src/index.ts
export type GlobResult = Record<string, () => Promise<{ [key: string]: any }>>;

export interface AstroVFile {
  data: { astro: { frontmatter: Record<string, any> } };
}

export type AstroRehypePlugin = (tree: HastNode, file: AstroVFile) => void;

export type RewriteLinksPluginOptions = {
  logging?: boolean;
  origin: string | URL;
  prefix?: string;
  removePublic?: boolean;
  fixMd?: boolean;
  addBlankTarget?: boolean;
  addTrailingSlash?: boolean;
  transforms?: [(node: HastElement, prop: "href" | "src") => string];
};
