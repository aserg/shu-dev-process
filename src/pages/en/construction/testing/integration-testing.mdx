---
title: Integration Testing
fragment: true
---

import FilterContent from "@contentFilter";

Integration testing is defined as a type of testing where software modules are integrated logically and tested as a group. It is essentially similar to unit testing, but instead of testing just one module at a time (such as a functiion or object), it tests how multiple modules interact with each other. The purpose of this level of testing is to expose defects in the interaction between these software modules when they are integrated.

Integration Testing focuses on checking data communication amongst these modules. Hence it is also termed as '**I & T**' (Integration and Testing), 'String Testing' and sometimes 'Thread Testing'.

### Why do Integration Testing?

Integration testing is important for a number of reasons:

- A Module, in general, is designed by an individual software developer whose understanding and programming logic may differ from other programmers. Integration Testing becomes necessary to verify the software modules work in unity.
- At the time of module development, there are wide chances of change in requirements by the clients. These new requirements may not be unit tested and hence system integration testing becomes necessary.
- Interfaces of the software modules with the database could be erroneous (incorrect).
- External Hardware interfaces, if any, could be erroneous.
- Inadequate exception handling could cause issues.

### Test Cases

As with unit testing, you are expected to write test cases for your integration testing. Integration test cases differ from other test cases in the sense it focuses mainly on the interfaces & flow of data/information between the modules. Here priority is to be given for the integrating links rather than the unit functions which should be already tested.

At Level 5, we are expecting manual integration testing, in a format similar to the following scenario, where this application has 3 modules 'Login Page', 'Mailbox' and 'Delete emails' and each of them is integrated logically:

| Test Case ID | Test Case Objective                                                  | Test Case Description                                   | Expected Result                                          | Actual Result                              |
| ------------ | -------------------------------------------------------------------- | ------------------------------------------------------- | -------------------------------------------------------- | ------------------------------------------ |
| 1            | Check the interface link between the Login and Mailbox module        | Enter login details and click on the Login button       | To be directed to the Mailbox                            | Directed to the Mailbox                    |
| 2            | Check the interface link between the Mailbox and Delete Mails Module | From Mailbox select the email and click a delete button | Selected email should appear in the Deleted/Trash folder | Selected email appears in the Trash folder |

This is based from reference [6]. Visit this resource to find out more about integration testing.

### Approaches

There are many different approaches to integration testing:

- Big Bang Approach
- Incremental Approach: which is further divided into the following
  - Top Down Approach
  - Bottom Up Approach
  - Sandwich Approach - Combination of Top Down and Bottom Up

#### Big Bang Testing

Big Bang Testing is an Integration testing approach in which all the components or modules are integrated together at once and then tested as a unit. This combined set of components is considered as an entity while testing. If all of the components in the unit are not completed, the integration process will not execute.

Advantages:

- Convenient for small systems.

Disadvantages:

- Fault Localization is difficult.
- Given the sheer number of interfaces that need to be tested in this approach, some interfaces links to be tested could be missed easily.
- Since the Integration testing can commence only after "all" the modules are designed, the testing team will have less time for execution in the testing phase.
- Since all modules are tested at once, high-risk critical modules are not isolated and tested on priority. Peripheral modules which deal with user interfaces are also not isolated and tested on priority.

#### Incremental Testing

In the Incremental Testing approach, testing is done by integrating two or more modules that are logically related to each other and then tested for proper functioning of the application. Then the other related modules are integrated incrementally and the process continues until all the logically related modules are integrated and tested successfully.

Incremental Approach, in turn, is carried out by two different Methods:

- Bottom-Up
- Top-Down

#### Bottom-Up

Bottom-up Integration Testing is a strategy in which the lower level modules are tested first. These tested modules are then further used to facilitate the testing of higher level modules. The process continues until all modules at top level are tested. Once the lower level modules are tested and integrated, then the next level of modules are formed. The diagram below illustrates this:

![Bottom-Up approach](https://www.guru99.com/images/bottom-up-integration-testing.png)

_Fig. 1: Bottom-Up approach. Retrieved from reference [6]_.

Advantages:

- Fault localization is easier.
- No time is wasted waiting for all modules to be developed unlike Big-bang approach.

Disadvantages:

- Critical modules (at the top level of software architecture) which control the flow of application are tested last and may be prone to defects.
- An early prototype is not possible.

#### Top-Down

Top Down Integration Testing is essentially the opposite of Bottom-Up. It is a method in which integration testing takes place from top to bottom following the control flow of software system. The higher level modules are tested first and then lower level modules are tested and integrated in order to check the software functionality. Stubs (dummy programs used to facilitate testing activity) are used for testing if some modules are not ready. The diagram below illustrates this:

![Top-Down approach](https://www.guru99.com/images/top-down-integration-testing.png)

_Fig. 2: Top-Down approach. Retrieved from reference [6]_.

Advantages:

- Fault Localization is easier.
- Possibility to obtain an early prototype.
- Critical Modules are tested on priority; major design flaws could be found and fixed first.

Disadvantages:

- Needs many Stubs.
- Modules at a lower level are tested inadequately.

#### Sandwich Testing

Sandwich Testing is a strategy in which top level modules are tested with lower level modules at the same time lower modules are integrated with top modules and tested as a system. It is a combination of Top-down and Bottom-up approaches therefore it is called 'Hybrid Integration Testing'. It makes use of both stubs as well as drivers:

![Sandwich approach](https://www.guru99.com/images/1/Hybrid-Integration.png)

_Fig. 3: Sandwich approach. Retrieved from reference [6]_.

**Stub**: Is called by the Module under Test.

**Driver**: Calls the Module to be tested.

Again, integration testing at Level 5 should be manual. Evaluate your system and experiment with these approaches, choose the one you feel most effective with on your system. It would be nice to see some Continuous Integration (CI) depending on the module (such as group assigments or work with clients). You can see more information about CI in the [Delivery Guidelines](/shu-dev-process/en/deployment/delivery).
