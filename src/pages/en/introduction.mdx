---
title: Introduction
---

**Welcome to the SHU Development Process!** Use the navigation sidebar <span class="desktop-only">on the left</span><span class="mobile-only">behind the three-bar button at the top of the page</span> to get started.

## What's this for?

The SHU development process details different project stages commonly seen in software engineering, and therefore throughout your studies. It's based on the generic process framework described by Pressman and Maxim [[1]](/shu-dev-process/en/introduction#references), and comprises five methodological stages: initiation, planning, modelling, construction and deployment. These main stages encompass a set of support steps: analysis and design related to modelling; code and test related to construction; and delivery, support and feedback for deployment. This site distills relevant information in a structured way, but you're also encouraged to read beyond the content.

### Scope

The SHU Development Process...

- 🧑‍🏫 Explains software development processes & artefacts, from **initiation** & **planning**, through to **modelling**, **construction**, & **deployment**.
- 📚 Serves as a useful resource for your assignments across many courses and modules.
- 📱 Has dark mode & mobile responsiveness!
- ❌ Won't teach you programming languages or detail any assignments. See your module content on Blackboard for that.
- ❌ Won't teach you how to write assignments or which books to read. Check out the library for skills & reading lists.

You can access these university services and more through [MyHallam](https://www.shu.ac.uk/myhallam).

### Navigation

This site is designed to be used with your chosen process flow and navigated however you see fit. Some examples are given below, however you should investigate methods such as waterfall, agile, prototype, spiral, extreme programming and others. Decide for yourself and with your team how they may or may not be applicable for your team and your project.

<div class="two-by-two">

        <div>
        #### Iterative
        Repeats one or more of the activities before processing to the next.
        <img src="/shu-dev-process/images/introduction/nav-iterative.png" alt="iterative approach"/>
        </div>

        <div>
        #### Evolutionary
        Executes activities in a cyclic or "circular" manner, with each iteration leading to a more complete version of the software.
        <img src="/shu-dev-process/images/introduction/nav-evolutionary.png" alt="evolutionary approach"/>
        </div>

        <div>
            #### Parallel
            Executes one or more activities in parallel. E.g. construction for one aspect may take place at the same time as deployment of another.
            <img src="/shu-dev-process/images/introduction/nav-parallel.png" alt="parallel approach"/>
        </div>

        <div>
            #### Ad-hoc
            Execute single activities on a one-off, exceptional basis. You might do this to refresh your memory or for individual and small assignments.
            <img src="/shu-dev-process/images/introduction/nav-adhoc.png" alt="ad-hoc approach"/>
        </div>
    </div>

## Features

<div class="two-by-two">

<div>

### Search

You can easily search the site by pressing forward slash <kbd>/</kbd> or <kbd>Ctrl/Cmd</kbd>+<kbd>K</kbd> on your keyboard, or by clicking the button at the top of the page. When you search, we'll try to show you the most relevant results, as well as useful suggestions.

![Search modal](/images/introduction/search-dialog.png)

</div>

<div>

### Search Shortcut

Want to search the site directly from your browser? Add our URL to your browser's search engines: [see how](https://www.howtogeek.com/114176/how-to-easily-create-search-plugins-add-any-search-engine-to-your-browser/).

```https://aserg.codeberg.page/shu-dev-process/search/?query=%s```

🍀 You can add the `lucky` parameter to be redirected to the first result:

```https://aserg.codeberg.page/shu-dev-process/search/?lucky=1&query=%s```

</div>

<div>
### Level Filters

You'll see everything needed for a process by default. If you only want to view information needed for your level of study, you can use the filter in the top-right of every page to select the level of detail you're after.

![Level Filter](/images/introduction/level-select.png)

</div>
<div>

### Sharing Content

Hover over any heading within the content and a sharing link will appear.

![Sharing link](images/introduction/sharing-hashtag.png)

Clicking on this link will open your device's sharing dialog, if one is supported, otherwise the link will be copied to your clipboard. Use this feature to easily share content with your coursemates!

<span class="mobile-only"> 
![Share sheet example](images/introduction/share-sheet-example.png)

_iOS share sheet_

</span>
<span class="desktop-only">
![Sharing link](images/introduction/sharing-windows.png)

_Windows sharing dialog_

</span>

</div>
<div>

### Easy Access

Access the site on the go by adding it to your devices homescreen:

- [Add a website to an Android or iPhone home screen - androidauthority.com](https://www.androidauthority.com/add-website-android-iphone-home-screen-3181682/)

![A2S dialog](images/introduction/a2h-dialog.png)

</div>

<div>

### Alternative Font

Use the font button in the top-right to toggle between the default font and one that you may find easier to read.

![Font button](images/introduction/font-button.png)

</div>

</div>

## References

[1] Pressman, R. S., & Maxim, B. R. (2010). Software engineering: A practitioner's approach (7th ed.). McGraw-Hill Higher Education.
