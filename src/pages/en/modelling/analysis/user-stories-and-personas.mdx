---
title: User Stories & Personas
description: A User Story is an informal, natural language description of one or more features of a software system.

---
import FilterContent from "@contentFilter";

## What is Expected?

<FilterContent options="4">
    At level 4, you should be able to complate basic stories and some acceptance criteria.
</FilterContent>
<FilterContent options="5">
    At level 5 you should be able to use basic stories, acceptance criteria, and personas.
</FilterContent>
<FilterContent options="6">
    At level 6 you should be able to use acceptance criteria, personas, and a requirements traceability matrix.
</FilterContent>

## Basic Story

A User Story is an informal, natural language description of one or more features of a software system. We write these under the perspective of a potential end user of our system.  
At Level 6, the expectation is that your User Stories are well defined and also fulfil the criteria using the template of;

-   As a
-   I want to
-   So that I can

Which enable us to identify "the actor", "the narrative" and "the goal".  
They are a useful tool to help us organise our understanding of the system and it's context.

## Acceptance Criteria

In the perfect world, people would understand each other at a glance and nothing could create confusion among them. But in the real world we have to come up with ways to communicate our ideas clearly so that our peers don’t misunderstand us.  
In software development, Acceptance Criteria help to properly set a client’s expectations for a product. Criteria for an app such as “I want my app to be awesome and popular with as many people as possible” don’t really tell us much; we eliminate misunderstandings between a client and a development team by referring to clearly defined Acceptance Criteria for user stories.

<FilterContent options="6">**At Level 6, Acceptance Criteria is required.**</FilterContent>

When writing Acceptance Criteria, it is not only important for eliciting the vision of a product from your client, but for the development process as well. It's natural that different people see the same problem from different angles. Clearly written criteria introduct a single solution to the functionality you intend to implement.

![User story template](images/modelling/Level_6_UserStoryTemplate.PNG)

#### What are Acceptance Criteria used for?

-   **To define boundaries:** Acceptance Criteria help development teams define the boundaries of a user story. In other words, acceptance criteria help you conform when the application functions as desired, meaning that a user story is completed.
-   **To reach consensus:** Having acceptance criteria synchronizes the development team with the client. The team knows exactly what conditions should be met, just as the client knows what to expect from the app.
-   **To serve as a basis for tests:** Last but not least, acceptance criteria are a cornerstone of positive and negative testing aimed at checking if a system works as expected.
-   **To allow for accurate planning and estimation:** Acceptance criteria scenarios allow for the correct division of user stories into tasks so user stories are correctly estimated and planned.

#### Who Writes Acceptance Criteria and When?

Your client or development team will be the ones to write your acceptance criteria, but as a rule, any criteria written by a **product owner (the client)** should be reviewed by a member of the development team to make sure that the criteria are clearly specified and that there are no technical constraints or inconsistencies from the development perspective. Such flow is an excellent way to collaborate if the product owner has some experience in software development and is aware of how to write project dopcumentation.

Remember that Acceptance Criteria should be specified **upfront** and **never** after the development stage has started. Therefore, a team and a product owner should agree on minimum deliverables that will meet the product owner's requirements.

If you prefer to assign writing acceptance criteria to the development team, then a requirements analyst, project manager or QA specialist should deal with this task, since they know the technology stack and the feasibility of features.

#### How to Write Acceptance Criteria

Acceptance Criteria is defined as 'what the story needs to do before it can be considered satisfactory'.
The common template for describing acceptance criteria using a scenario-oriented approach is the

-   Given
-   When
-   Then

format that is derived from **behaviour driven development**. This format is used for writing acceptance criteria tests that ensure that all the specification requirements are met. This format is convenient for humans (since it’s written in a familiar cause-and-effect manner) as well as for automated testing tools like Cucumber and RSpec. For example, when we build a website that has two types of users ‒ logged-in users and guests ‒ we’re likely to write the following acceptance criteria for a user story that defines the sign-in feature for a logged-out user:

> **As** a logged-out user  
> **I want** to be able to sign in to a website  
> **So that** I can find access my personal profile  
> **Scenario:** System user signs in with valid credentials  
> “Given I’m a logged-out system user  
> and I’m on the Sign-In page  
> When I fill in the “Username” and “Password” fields with my authentication credentials  
> and I click the Sign-In button  
> Then the system signs me in”

This template helps you to reduce the time spent on writing test cases since you describe the systems' behaviour upfront. We prefer writing acceptance criteria with the first-person “I” since it helps us talk from a user’s perspective and keep a user’s needs in mind.

Defining the required amount of detail to be included here varies based on your team, your project and program.  
Sometimes you may be expected to include what is known as **Predecessor Criteria**.  
You can also write your Acceptance Criteria in the form of bullet points, but communicate with your team and find out the way you want to structure them in advance.

Here are a few tips that’ll help you write great acceptance criteria:

-   Keep your criteria well-defined so any member of the project team understands the idea you’re trying to convey.
-   Keep the criteria realistic and achievable. Define the minimum piece of functionality you’re able to deliver and stick to it. On the other hand, don’t try to describe every detail since you risk cluttering up your backlog and getting buried under many small tasks.
-   Coordinate with all the stakeholders so your acceptance criteria are based on consensus.
-   Create measurable criteria that allow you to adequately estimate development time so you’re able to stay within budget and time constraints.
-   Consider providing checklists that enable you to see what user stories are covered with acceptance criteria.

![Acceptance criteria](https://rubygarage.s3.amazonaws.com/uploads/article_image/file/608/acceptance-criteria.jpg)  
_Figure - RubyGarage Acceptance Criteria: Unclear vs Clear_

<FilterContent options="5, 6">

## Personas

Personas are fictional, but realistic representations of target users of your product/software. Persona is created to visualize better the target users, who you are trying to reach. It helps to make right decisions about product features, navigation, interactions, visual design and much more.
These fictional characters are often named, and include a picture to represent them. The purpose of Personas is to make the users seem more real, to help designers keep realistic ideas of users throughout the design process.

A Persona also includes specific characteristics, demographics, and experience levels from a user profile, for example, a specific hardware and software configuration. Additional information in personas are personal details such as behaviors, attitudes, motivations, and goals.
They can range from anywhere between a single paragraph and a full page.

### What Does A Persona Look Like?

Below is an example of what a Persona could look like. You should try to produce something similar for your own projects:

![An example Persona](images/modelling/persona.png)

*Fig. 1: An example Persona of a Regional Manager. Retrieved from reference [1].*

This is a Persona of a Regional Manager, looking to find a website that will allow her to plan her business trips easier. It includes sections on personal details and a bio, demographics, goals, frustrations and motivations.
Your User Personas do not necessarily have to include all of the above, but they should be detailed enough to make them seem like they are a real person, and a real target audience for your product.

### What Is A User Scenario?

A User Scenario and a Persona go hand in hand and one should not be produced without the other. A Scenario is eccentially a more detailed, descriptive version of a Persona.
Like Personas, User Scenarios are a way to allow developers to try to understand what users want, how they feel and what they want from the product.

User Scenario tends to include who the user is and what their goal is at that moment. Generally, Scenarios are concise and represent a snapshot of the user experience.
Many design teams believe the main advantage of having user scenarios is that once we establish what the user’s goal is, it becomes easier to define how the user would go about reaching that goal.
They are about tracing a user’s steps in order to complete a task and validating aspects of the design that might have otherwise overlooked.

Some things a Scenario might include are:

- Who the user is
- The situation that drives users to seek the product
- A specific task or goal the user has
- Information regarding user’s income and spending
- The path to completion of a task
- Points of friction or stress in daily life or in the user experience

### What Does A User Scenario Look Like?

Below is an example of a User Scenario for a Supply Manager:

*“Jeremy, 52, a senior manager for a medical supplies company, needs constantly updated information on purchasing-related issues while he travels between work and hospital sites so he can use/allocate resources optimally. He’s highly skilled, organized and diligent.*

*However, with recent layoffs he now struggles to manage his workload and is too drained to enjoy his career. He strains to handle tasks which his former assistant previously performed, stays current with issues and investigates supply-chain problems, while he tries to find alternatives that would be more economical in the financial climate.*

*He wants something convenient like an app to take him straight to only the most relevant updates and industry news, including current information feeds about share prices, tariffs on foreign suppliers, budget decisions in local hospitals and innovations in the medical devices he handles (mostly lung and cardiovascular products).*

*Instead of continuing to liaise with three other managers and spending an hour generating one end-of-day report through the company intranet, he’d love to have all the information he needs securely on his smartphone and be able to easily send real-time screenshots for junior staff to action and file and corporate heads to examine and advise him about.”*

This is from reference [2].

<FilterContent options="5"> At Level 5 it is expected that you complete at least one Persona and one User Scenario for your projects.
</FilterContent> If you complete two (recommended), the two different Personas and Scenarios should be about different aspects of the system, and should be two different fictional characters. For example, June could be a manager who struggles with technology, Ben could be an employee who has eye sight problems.
Each one should explore a functional aspect of the system, to get a feel of what path a user is likely to take to complete their goal.

The Personas and Scenarios should link together, i.e. June's Persona must link with June's Scenario. They should also be consistent with your User Stories, as well as your Use Case models and/or Requirements Specification.
If you get stuck creating either a Persona or Scenario, go back and look at your User Stories, and try to get an idea of who the target audience for your product is likely to be.

To help you out, a basic template of a Persona is provided [here](/shu-dev-process/files/modelling/persona_template.docx). Feel free to download and use this as a starting point for your own personas.

</FilterContent>

<FilterContent options="6">
## Requirement Traceability Matrix

This is a document that is used to demonstrate relationships between requirements and other artefacts.  
It is the ability to connect your requirements to other artefacts, such as different types of software tests or bugs. It's used to track requirements, and prove that requirements have been fulfilled.

- [Download .docx template](/shu-dev-process/files/modelling/Level%206%20ReqTraceability.docx)

![Requirement Traceability Matrix](images/modelling/Level_6_RTM_Template.PNG)

</FilterContent>

<FilterContent options="5, 6">
## References

- RubyGarage - Clear Acceptance Criteria and Why it's Important - https://rubygarage.org/blog/clear-acceptance-criteria-and-why-its-important
- 99 Designs. How to create a user persona. https://99designs.co.uk/blog/business/how-to-create-user-personas/
- Just In Mind. How to design user scenarios: best practices and examples. https://www.justinmind.com/blog/how-to-design-user-scenarios/

</FilterContent>
