---

title: Architecture Overview
description: An architecture overview is an document that visually represents how the users, components, and services related to your system interact.
---

An architecture overview is an document that visually represents how the users, components, and services related to your system interact. It can be used by stakeholders to discuss the decisions you have made and any potentials trade-offs that will need to me made. As an informal document there is no one correct format for an architecture overview, though some guidelines are given below.

## Diagram Elements

The diagram is visual, so annotations between elements are normally kept to a minimum. You may choose to use icons to represent components or simply label them with text. If you have a lot of elements, consider using colours and shapes to differentiate between elements. A key may be useful to explain what these represent.

![library example diagram](images/modelling/architecture-overview.jpg)

- **Users:** It's important to represent the consumers of your system. These are often people interacting through an interface but they may be other software services that consume your system through an API you have created.
- **System Boundary:** At the very least, you will have users that interact with your system. You may also have external systems that you rely on, such as a weather API, user authentication, or an external database.
- **Internal Blocks:** Internal blocks can represent processes, actions, or models that take place within your system. It can be useful to group related elements together.
- **External Services:** An external system is one that your system consumes. You may not have ownership or control of them. They should be represented outside of your system boundary.

## C4 Model

The [C4 Model](https://c4model.com/) is a notation agnostic way of structuring and linking your system diagrams based on their "zoom" levels, or which stakeholder(s) are involved. You can consider using this when presenting your documents. Four core level diagrams are described:

- **Level 1:** A **System Context** diagram provides a starting point, showing how the software system in scope fits into the world around it.
- **Level 2:** A **Container** diagram zooms into the software system in scope, showing the high-level technical building blocks.
- **Level 3:** A **Component** diagram zooms into an individual container, showing the components inside it.
- **Level 4:** A **code** (e.g. UML class) diagram can be used to zoom into an individual component, showing how that component is implemented.

Along with the suggested structure is a [diagram checklist](https://c4model.com/review/) that, while not to be used as the be all and end all of diagram creation, is comprehensive enough to guide you when reviewing your documents.

Any tool may be used to create a C4 model, though when using C# you might like to inline your diagrams with the rest of your code using the open-source [C4# library](https://github.com/8T4/c4sharp/wiki).

## Architectural Decisions Records

An architectural decision record is a document that captures an important architecture decision made along with its context and consequences. While not formally required in your course, ADRs can provide context and justification for your overview diagram. They are offered here as a suggestion of a format you can use when you write about your chosen architecture.

* [ADR Github](https://adr.github.io/)
* [Markdown example](https://github.com/adr/madr)
* [Templates](https://github.com/joelparkerhenderson/architecture-decision-record)


## Tools

There are a number of tools you can use to create an architecture overview. Explore the examples below to find out which might be best for your workflow:

* [diagrams.net](https://diagrams.net/)
* [Microsoft Visio](https://www.microsoft.com/en-gb/microsoft-365/visio/flowchart-software)
* [mermaid-js](https://mermaid-js.github.io/mermaid/#/) (implemented on both GitHub and Gitea)
* [TextUML](http://abstratt.github.io/textuml/)