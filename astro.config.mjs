import { defineConfig } from "astro/config";
import preact from "@astrojs/preact";
import react from "@astrojs/react";
import sitemap from "@astrojs/sitemap";
import mdx from "@astrojs/mdx";
import defaultLayoutPlugin from "./custom-astro-plugins/remark/default-layout.mjs";
import remarkMermaid from "astro-diagram/remark-mermaid";
import remarkGfm from "remark-gfm";
import lazyLoadPlugin from "rehype-plugin-image-native-lazy-loading";
import plaintextFrontmatter from "./custom-astro-plugins/rehype/plaintext-frontmatter/index";
import rewriteLinks from "./custom-astro-plugins/rehype/rewrite-links/index";
import makeCodeFocusable from "./custom-astro-plugins/rehype/make-scrollable-code-focusable/index";
import headingFragmentLinks from "./custom-astro-plugins/rehype/heading-fragment-links/index";

// ref: https://astro.build/config

//import.meta.env doesn't work this early in the build process, so it can't be used
//regular environment variables are set differently on Windows & Linux, so the build command is checked instead
const IS_DEV = process.argv.indexOf("dev") != -1;
const IS_DEV_DEPLOY =
  process.env.NODE_ENV == "devdeploy" || process.argv.indexOf("--devdeploy");

let SITE = "https://aserg.codeberg.page/shu-dev-process/";

// if (IS_DEV_DEPLOY) {
//   // SITE += "@pages-develop/";
// }

if (IS_DEV && !IS_DEV_DEPLOY) {
  SITE = "http://localhost:3000/shu-dev-process/";
}

const BASE = new URL(SITE).pathname;

// These lists are ordered and later plugins may depend on the results of previous ones
const REMARK_PLUGINS = [defaultLayoutPlugin, remarkGfm, remarkMermaid];
const REHYPE_PLUGINS = [
  rewriteLinks({
    logging: true,
    origin: SITE,
    prefix: BASE,
    removePublic: true,
    addBlankTarget: true,
    addTrailingSlash: true,
    fixMd: true,
  }),
  makeCodeFocusable,
  lazyLoadPlugin,
  headingFragmentLinks,
  plaintextFrontmatter(),
];

const INTEGRATIONS = [
  // Enable Preact to support Preact JSX components.
  //keep compress last in this list
  preact(),
  react(),
  mdx({ extendMarkdownConfig: true }),
  sitemap(),
];

// https://astro.build/config
export default defineConfig({
  site: SITE,
  base: BASE,
  output: "static",
  trailingSlash: "ignore",
  markdown: {
    drafts: true,
    extendDefaultPlugins: true,
    remarkPlugins: REMARK_PLUGINS,
    rehypePlugins: REHYPE_PLUGINS,
    shikiConfig: {
      theme: "nord",
      wrap: true,
    },
  },
  integrations: INTEGRATIONS,
  prefetch: true,
});
