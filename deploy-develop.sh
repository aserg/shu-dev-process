#!/usr/bin/env bash
# Run this script to deploy the current branch to Codeberg Pages page-develop branch
branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')

./deploy.sh -s ${branch} -d page-develop -t
