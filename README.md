# shu-dev-process

<a href="https://aserg.codeberg.page/shu-dev-process/" target="_blank" title="SHU Development Process">
    <img src="https://img.shields.io/badge/View%20Online%20%E2%86%97-b6004c" alt="View as HTML Badge"/>
</a>

We are creating a software development process that can be followed by students at all levels as they work on both individual and group assessments. We're doing this because we want our students to use good practice, to understand the software lifecycle and to become familiar with industry-standard tools.

This version uses the [Astro](https://astro.build) framework to build static HTML from markdown files. If you're not familiar with Astro, read the [Getting Started guide](https://docs.astro.build/en/getting-started/).

## Contents of the Repo

- `.vscode/` - Config files for VS Code: recommended extensions, launch profiles.
- `public/` - Static files that will be published straight to the output.
- `src/` - Templates, layouts, and components in `.astro`,  `html`, and `.tsx` format.
- `src/pages/` - The source markdown files used to create content for the site. Folder structure here relates directly to final URL path structure.
- `src/config.ts` - Theme specific config options.
- `astro.config.mjs` - configuration options and integrations for the Astro build process.
- `package.json` - Lists dependencies and scripts.

## Prerequisites

- [Node.js](https://nodejs.org/)
- Bash shell, if running deploy scripts

### Dependencies:

- [Astro.build](https://astro.build) and integrations:
  - [MDX](https://docs.astro.build/en/guides/integrations-guide/mdx/)
  - [sitemap](https://docs.astro.build/en/guides/integrations-guide/sitemap/)
  - [astro-diagram](https://code.juliancataldo.com/component/astro-diagram/)
  - [minisearch](https://barnabas.me/articles/client-side-search/)

See `package.json` for full details.

## Installation

1. Clone this repo.
2. From the root folder, run `npm install`.

## Development Usage

This repo is modified from Astro's [official docs theme](https://github.com/withastro/astro/tree/main/examples/docs). It's important to note:

- **Styling:** *The theme's look and feel is controlled by a few key variables that you can customize yourself. You'll find them in the `src/styles/theme.css` CSS file, but edits should be made in `src/styles/shu-styles.css`.*
- **Frontmatter:** *Astro uses frontmatter in Markdown pages to choose layouts and pass properties to those layouts. You can customize the page in many different ways to optimize SEO and other things. For example, you can use the `title` and `description` properties to set the document title, meta title, meta description, and Open Graph description.*
- **Navigation:** *The sidebar navigation is controlled by the `SIDEBAR` variable in your `src/config.ts` file. You can customize the sidebar by modifying this object.*

1. Run `serve-local.sh` or `npm run astro dev` from the root of the repository (this will enable hot reload). If you're using VS Code, you can also press `Ctrl/Cmd + F5`.
2. Open your browser to `https://localhost:3000/shu-dev-process/`.
3. Edit your files and check the browser updates as expected.

### Pushing Changes (Branch Use)

This is how branches are being used:

- `main` - Code is never directly edited on this branch. It is used only to store the release history that we publish.
- `pages` - Code is never directly edited on this branch. It is the live published version of the site.
- `pages-develop` - Code is never directly edited on this branch. It is a preview version of the site.
- `develop` - An integration branch for features. While you're working towards a release, new features will be forked to and from this branch.
- `[named-feature]` - Feature branches are forked from the develop branch. This is where the bulk of your code should be written.

### Testing the Site:

This is detailed in the [Testing Guide](TESTING-GUIDE.md).

## Content Considerations

This is detailed in the [Writing Guide](WRITING-GUIDE.md).

## Deploying Content

The site is published via [Codeberg Pages](https://docs.codeberg.org/codeberg-pages/).

### Checking

Run `deploy-develop.sh` to generate static HTML files from your current branch and automatically push them to the `pages-develop` branch. They'll then be served through at this address for you and the team to check your changes:

https://aserg.codeberg.page/shu-dev-process/@page-develop/

### Publishing to Students

Run `deploy.sh` to generate static HTML files from the `main` branch and automatically push them to the `pages` branch. They'll then be served through at the below address. 

**⚠ This address is distributed to students**, so content should be checked before it is pushed to the live `pages` branch.

https://aserg.codeberg.page/shu-dev-process/

## Roadmap & Issues

Issues and features are tracked in the [shu-dev-process repo](https://codeberg.org/aserg/shu-dev-process/issues).