# Testing Guide

The following need to be considered when developing code:

- **Accessibility:** W3C maintains a [list of tools](https://www.w3.org/WAI/ER/tools/) you can use to check for WCAG2.1 compliance.
- **Responsiveness:** does the site display correctly on a variety of screen sizes and ratios? Use your browser developer tools to check.
- **Print layout:** does the stylesheet for the `print` layout have any errors? Use the DevTools or print dialog in your browser to check.

### Running Tests

Run `npm run run-tests` to check the built site for accessibility, integration, and unit testing.
When using the `deploy.sh` script, pass `-t` as an argument to trigger tests before deployment. 
